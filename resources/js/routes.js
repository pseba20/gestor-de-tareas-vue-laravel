const Home = () => import("./components/Home.vue");
const Login = () => import("./components/Login.vue");
const Register = () => import("./components/Register.vue");
const Contacto = () => import("./components/Contacto.vue");
//importamos los componentes para el task
const Mostrar = () => import("./components/task/Mostrar.vue");
const Crear = () => import("./components/task/Crear.vue");
const Editar = () => import("./components/task/Editar.vue");

export const routes = [
    {
        name: "home",
        path: "/",
        component: Home,
        beforeEnter: (to, form, next) =>{
            axios.get('/api/athenticated').then(()=>{
                next()
            }).catch(()=>{
                return next({ name: 'login'})
            })
        }
    },

    {
        name: "login",
        path: "/login",
        component: Login,
    },

    {
        name: "register",
        path: "/register",
        component: Register,
    },
    {
        name: "mostrarTask",
        path: "/tasks",
        component: Mostrar,
        beforeEnter: (to, form, next) =>{
            axios.get('/api/athenticated').then(()=>{
                next()
            }).catch(()=>{
                return next({ name: 'login'})
            })
        }
    },
    {
        name: "crearTask",
        path: "/crear",
        component: Crear,
        beforeEnter: (to, form, next) =>{
            axios.get('/api/athenticated').then(()=>{
                next()
            }).catch(()=>{
                return next({ name: 'login'})
            })
        }
    },
    {
        name: "editarTask",
        path: "/editar/:id",
        component: Editar,
        beforeEnter: (to, form, next) =>{
            axios.get('/api/athenticated').then(()=>{
                next()
            }).catch(()=>{
                return next({ name: 'login'})
            })
        }
    },
    {
        name: "contacto",
        path: "/contacto",
        component: Contacto,
        beforeEnter: (to, form, next) =>{
            axios.get('/api/athenticated').then(()=>{
                next()
            }).catch(()=>{
                return next({ name: 'login'})
            })
        }
    },

];
