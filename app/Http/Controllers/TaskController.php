<?php

namespace App\Http\Controllers;

use App\Models\Task;
use App\Models\Tags;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Auth;
use App\Jobs\SendEmail;
use Carbon\Carbon;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $tasks = Task::with('status', 'users', 'priority','tags')
        ->orderby('created_at', 'desc')->get();
        return response()->json($tasks);
    }

    public function alltags()
    {
        $tags = tags::orderby('name', 'asc')->get();
        return response()->json($tags);
    }

    public function status_total($id){
        $task = Task::where('estado_id', $id)->get()->count();
        return response()->json($task);
    }
    public function status_count()
    {
        $task = Task::with('status')->select('id', 'estado_id')->groupby('estado_id')->get();
        return response()->json($task);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function notify()
    {
        // $cinco_dias_antes = Carbon::now()->subDay(5)->format('Y-m-d');
        // $un_dia_antes = Carbon::now()->subDay(1)->format('Y-m-d');

        $hoy = Carbon::now()->format('Y-m-d');

        $task = Task::with('users')->where('fecha_vence', '>=',  $hoy)->get();

        foreach($task as $t){

            $mail_to = $t->users->email;
            $vence = 'La tarea '.$t->titulo. ' vence el dia '.$t->fecha_vence;
            SendEmail::dispatch($mail_to, $vence);
        }

    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task = Task::create($request->post());

        //Validamos que existan tags agregados
        if(isset($request->tags)){

            $tmp= array();
            foreach($request->tags as $tag){
                array_push( $tmp, $tag['id']);
            }
            $task->tags()->sync($tmp);
        }


        return response()->json(['task' => $task]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task = Task::with('status','users','priority','tags')->where('id', $id)->first();
        return response()->json($task);
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {


        if(isset($request->tags)){

            $tmp= array();
            foreach($request->tags as $tag){
                array_push( $tmp, $tag['id']);
            }
            // $task = Task::with('status','users','priority','tags')->where('id', $task->id)->first();
            $task->tags()->sync($tmp);
        }
        $task->fill($request->post())->save();
        return response()->json(['task' => $task]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        $tmp = $task;
        $task->delete();
        return response()->json(['task' => $tmp]);
    }
}
