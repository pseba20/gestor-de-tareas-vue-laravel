<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
    use HasFactory;
    protected $table = "tags";
    protected $fillable = ['name'];


    //Relationships Many to Many
    public function tasks(){
        return $this->belongsToMany(Task::class, 'tasks_tags','task_id');
    }
}
