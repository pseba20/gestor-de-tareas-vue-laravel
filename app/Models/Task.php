<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;
    protected $fillable = ['titulo', 'tarea', 'estado_id','user_id', 'prioridad_id', 'fecha_vence'];

    public function status()
    {
        return $this->belongsTo(Status::class,'estado_id');
    }

    public function priority()
    {
        return $this->belongsTo(Prioridad::class,'prioridad_id');
    }

    public function users()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    //Relationships Many to Many
    public function tags()
    {
        return $this->belongsToMany(Tags::class, 'tasks_tags','task_id', 'tag_id');
    }
}
