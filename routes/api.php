<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\StatusController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PrioridadController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::resource('task', TaskController::class); //->only(['index', 'store','update','show','destroy']);
Route::get('status_count', [TaskController::class,'status_count']); //->only(['index', 'store','update','show','destroy']);
Route::get('status_total/{id}/', [TaskController::class,'status_total']); //->only(['index', 'store','update','show','destroy']);
Route::get('all_tags', [TaskController::class,'alltags']); //->only(['index', 'store','update','show','destroy']);
Route::get('notify', [TaskController::class,'notify']); //->only(['index', 'store','update','show','destroy']);

Route::resource('status', StatusController::class); //->only(['index', 'store','update','show','destroy']);
Route::resource('users', UserController::class); //->only(['index', 'store','update','show','destroy']);
Route::resource('priority', PrioridadController::class); //->only(['index', 'store','update','show','destroy']);


Route::middleware('auth:sanctum')->get('/athenticated', function () {
    return true;
});
Route::post('register', [RegisterController::class, 'register']);
Route::post('login', [LoginController::class, 'login']);
Route::post('logout', [LoginController::class, 'logout']);
