<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Model::unguard();
        // $this->call('StatusSeeder');
        // Model::reguard();

        $this->call([
        	PrioridadSeeder::class,
        	StatusSeeder::class,
        	UserSeeder::class,
        	TaskSeeder::class,
        	TagsSeeder::class,
        	TaskTagsSeeder::class,

    	]);
    }
}
