<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Prioridad;
class PrioridadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('priority')->delete();

        // $priotity = array(
        //     array(
        //         'id' => 1,
        //         'name' => 'Baja',
        //     ),

        //     array(
        //         'id' => 2,
        //         'status' => 'Normal',
        //     ),


        //     array(
        //         'id' => 3,
        //         'status' => 'Urgente',
        //     ),

        // );
        // DB::table('priority')->insert($priotity);

        Prioridad::factory()->count(5)->create();

    }
}
