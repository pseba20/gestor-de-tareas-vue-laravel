<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Task;
use App\Models\Tags;

class TaskTagsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //Se crean 15 estudiantes
        Task::factory()->times(15)->create();
        //Se crean 8 cursos
        Tags::factory()->times(8)->create()->each( function($task){
            $task->tasks()->sync(
                //Cada curso es tomado por 3 estudiantes
                Task::all()->random(3)
            );
        });
    }
}
