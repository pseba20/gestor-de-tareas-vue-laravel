<?php

namespace Database\Seeders;
use App\Models\User;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('users')->delete();

        // $users = array(
        //     array(
        //         'id' => 1,
        //         'name' => 'Usuario 1',
        //         'email' => 'Usuario.1@mail.com',
        //         'password' =>  bcrypt('123456'),
        //     ),

        //     array(
        //         'id' => 2,
        //         'name' => 'Usuario 2',
        //         'email' => 'Usuario.2@mail.com',
        //         'password' =>  bcrypt('123456'),
        //     ),


        //     array(
        //         'id' => 3,
        //         'name' => 'Usuario 3',
        //         'email' => 'Usuario.3@mail.com',
        //         'password' =>  bcrypt('123456'),
        //     ),
        // );
        // DB::table('users')->insert($users);


       //UserFactory::factory(10)->create(10);
       User::factory()->count(5)->create();
    }
}
