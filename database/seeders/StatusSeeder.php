<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Status;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('status')->delete();

        // $status = array(
        //     array(
        //         'id' => 1,
        //         'status' => 'Nueva',
        //         'orden' => '1',
        //     ),

        //     array(
        //         'id' => 2,
        //         'status' => 'En proceso',
        //         'orden' => '2',
        //     ),


        //     array(
        //         'id' => 3,
        //         'status' => 'Finalizada',
        //         'orden' => '3',
        //     ),


        //     array(
        //         'id' => 4,
        //         'status' => 'En revisión',
        //         'orden' => '4',
        //     ),

        //     array(
        //         'id' => 5,
        //         'status' => 'Resuelta',
        //         'orden' => '5',
        //     ),

        // );
        // DB::table('status')->insert($status);
        Status::factory()->count(5)->create();
    }
}
