<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Task;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('tasks')->delete();

        // $tasks = array(
        //     array(
        //         'id' => 1,
        //         'titulo' => 'Reparar cableado',
        //         'tarea' => 'Se debe reparar el cableado que conecta al laboratorio de analisis clínicos con el datacenter, actualmente esta sin conexión',
        //         'estado_id' =>  1,
        //         'user_id' =>  1,
        //         'prioridad_id' =>  1,
        //     ),

        //     array(
        //         'id' => 2,
        //         'titulo' => 'Reconfigurar equipos',
        //         'tarea' => 'Se deberán reconfigurar los equipos en dicha sala. ',
        //         'estado_id' =>  1,
        //         'user_id' =>  2,
        //         'prioridad_id' =>  1,

        //     ),
        //     array(
        //         'id' => 3,
        //         'titulo' => 'Reordenar rack',
        //         'tarea' => 'Se debe reordenar los equipos dentro del rack, hay cables desordenados y desconectados.',
        //         'estado_id' =>  1,
        //         'user_id' =>  1,
        //         'prioridad_id' =>  1,

        //     ),

        //     array(
        //         'id' => 4,
        //         'titulo' => 'Deploy del sistema',
        //         'tarea' => 'Luego del QA se debe proceder a pasar a producción el sistema.',
        //         'estado_id' =>  2,
        //         'user_id' =>  1,
        //         'prioridad_id' =>  1,

        //     ),

        //     array(
        //         'id' => 5,
        //         'titulo' => 'Backups MySQL',
        //         'tarea' => 'Backup del mes pendiente.',
        //         'estado_id' =>  2,
        //         'user_id' =>  1,
        //         'prioridad_id' =>  3,

        //     ),

        //     array(
        //         'id' => 6,
        //         'titulo' => 'Backup SQL Server',
        //         'tarea' => 'Backup del mes pendiente.',
        //         'estado_id' =>  3,
        //         'user_id' =>  1,
        //         'prioridad_id' =>  1,

        //     ),


        // );
        // DB::table('tasks')->insert($tasks);

        Task::factory()->count(50)->create();

    }
}
