<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

use App\Models\Prioridad;
use App\Models\Status;
use App\Models\User;

class TaskFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'titulo' => $this->faker->title,
            'tarea' => $this->faker->text,
            'created_at' => now(),
            'prioridad_id' => rand(1,5),
            'estado_id' => rand(1,5),
            'user_id' => User::factory(),
            'fecha_vence' => now(),
        ];
    }
}
